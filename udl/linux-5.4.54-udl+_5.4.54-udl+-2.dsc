Format: 1.0
Source: linux-5.4.54-udl+
Binary: linux-image-5.4.54-udl+, linux-headers-5.4.54-udl+, linux-libc-dev, linux-image-5.4.54-udl+-dbg
Architecture: amd64
Version: 5.4.54-udl+-2
Maintainer: sr <sr@wbxz2>
Homepage: http://www.kernel.org/
Build-Depends: bc, rsync, kmod, cpio, bison, flex | flex:native, libelf-dev:native, libssl-dev:native
Package-List:
 linux-headers-5.4.54-udl+ deb kernel optional arch=amd64
 linux-image-5.4.54-udl+ deb kernel optional arch=amd64
 linux-image-5.4.54-udl+-dbg deb debug optional arch=amd64
 linux-libc-dev deb devel optional arch=amd64
Checksums-Sha1:
 3f6c92743649c945764f3dea77b8eeb5e6ca98c7 376080575 linux-5.4.54-udl+_5.4.54-udl+.orig.tar.gz
 a1420946f97f6236baf2f21cd75a1d0800d87a7c 469701 linux-5.4.54-udl+_5.4.54-udl+-2.diff.gz
Checksums-Sha256:
 2a9b6edb9c0fcf1a201e7262ffaff77d72c40845b186f685136b51b6796594df 376080575 linux-5.4.54-udl+_5.4.54-udl+.orig.tar.gz
 0147c0048ee67fa8b06757c3069f62b69f70a62e9cc9516abfef3d8f5b9d7ad3 469701 linux-5.4.54-udl+_5.4.54-udl+-2.diff.gz
Files:
 cbaef33b8a04dc054fc81eb4c19ebf15 376080575 linux-5.4.54-udl+_5.4.54-udl+.orig.tar.gz
 3317a653d1b345e982bca9e79af66b75 469701 linux-5.4.54-udl+_5.4.54-udl+-2.diff.gz
